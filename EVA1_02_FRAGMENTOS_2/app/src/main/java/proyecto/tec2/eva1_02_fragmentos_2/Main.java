package proyecto.tec2.eva1_02_fragmentos_2;

import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class Main extends AppCompatActivity {

    blueFragment bfBlue;
    redFragment rfRed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(this, ":'V", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAttachFragment(android.support.v4.app.Fragment fragment) {
        super.onAttachFragment(fragment);
        if(fragment.getClass() == redFragment.class){
            rfRed = (redFragment) fragment;
        } else if(fragment.getClass() == blueFragment.class){
            bfBlue = (blueFragment) fragment;
        }
    }

    public void onMsgFromFragToMain(String sSender, String sValor){
        Toast.makeText(this, "Fragmento: "+ sSender, Toast.LENGTH_SHORT).show();
        if (sSender.equals("RED_FRAG")){
            //HACEMOSALGO EN EL FRAGMENTO AZUL
            Toast.makeText(this, sSender + sValor, Toast.LENGTH_SHORT).show();
        } else if(sSender.equals("BLUE_FRAG")){
            //HACEMOS ALGO EN EL FRAGMENTO ROJO
            rfRed.onMsgFromMAin("Mensaje de " + sSender + " = " + sValor);
        }
    }
}
