package proyecto.tec2.eva1_02_fragmentos_2;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;


/**
 * A simple {@link Fragment} subclass.
 */
public class blueFragment extends Fragment {


    Main mMain;
    Context cContext = null;
    String sItems[] = {"Alola","Hola","Hello","AnnyongHaseyo","Konishiwa","asdjajskd","skajdhads","uuerire","kjdhsodifi",
        "sdlfusoidfoileerjwehrio","askjdhkashjaksdasdkjah","uwqeqwe","susfahf","aiosduiof","aufyefulsdf"};
    public blueFragment() {
        // Required empty public constructor

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cContext = getActivity();
        mMain = (Main)getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //CREAR EL LAYOUT
        LinearLayout llBlue = (LinearLayout) inflater.inflate(R.layout.fragment_blue, container, false);
        ListView lvBlue = (ListView) llBlue.findViewById(R.id.lvBlue);
        ArrayAdapter<String> aaDatos = new ArrayAdapter<String>(
                //CONTEXTO, FORMATO , ORIGEN
                cContext, android.R.layout.simple_list_item_1, sItems
        );
        lvBlue.setAdapter(aaDatos);
        lvBlue.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mMain.onMsgFromFragToMain("BLUE_FRAG",sItems[i]);
            }
        });
        // Inflate the layout for this fragment
        return llBlue;
    }

}
