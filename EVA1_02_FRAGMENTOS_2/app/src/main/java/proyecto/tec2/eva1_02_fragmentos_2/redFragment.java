package proyecto.tec2.eva1_02_fragmentos_2;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class redFragment extends Fragment {

    Context cContext = null;
    TextView tvRed;
    Main mMain;

    public redFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMain = (Main)getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LinearLayout llRed = (LinearLayout) inflater.inflate(R.layout.fragment_red, container, false);
        Button btnRed = (Button)llRed.findViewById(R.id.btnRed);
        tvRed = (TextView)llRed.findViewById(R.id.tvRed);
        btnRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sMsg =  new Date().toString();
                tvRed.setText(sMsg);
                mMain.onMsgFromFragToMain("RED_FRAG", sMsg);
            }
        });
        // Inflate the layout for this fragment
        return llRed;
    }

    public void onMsgFromMAin(String sVal){
        tvRed.setText(sVal);
    }

}
