package com.example.angelcordova.eva2_practica1;

import android.content.Intent;
import android.graphics.BlurMaskFilter;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import jp.wasabeef.blurry.Blurry;
import jp.wasabeef.blurry.internal.Blur;

public class MostrarDetalle extends AppCompatActivity {

    /*LinearLayout llDetalle;
    TextView tvNombr, tvDescr, tvCalle, tvCol, tvTel;
    Intent inDial;*/
    int iIndex;
    detalleFragment dfDetalle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostrar_detalle);

        Intent inDatosShow = getIntent();
        int iImgF = inDatosShow.getIntExtra("IMAGEN",R.drawable.cafeloisl);
        String sNm = inDatosShow.getStringExtra("NOMBRE");
        String sDsc = inDatosShow.getStringExtra("DESCRIP");
        String sClle = inDatosShow.getStringExtra("DIRECCION");
        String sCol = inDatosShow.getStringExtra("COLONIA");
        String sTlf = inDatosShow.getStringExtra("TELEFONO");
        dfDetalle.onMsgFromMain(iImgF,sNm,sDsc,sClle,sCol,sTlf);

        /*llDetalle =(LinearLayout) findViewById(R.id.activity_mostrar_detalle);
        tvNombr = (TextView) findViewById(R.id.tvNombree);
        tvDescr = (TextView) findViewById(R.id.tvDescrip);
        tvCalle = (TextView) findViewById(R.id.tvCallee);
        tvCol = (TextView) findViewById(R.id.tvColonia);
        tvTel = (TextView) findViewById(R.id.tvTelef);


        Intent inDatosShow = getIntent();
        int iImgF = inDatosShow.getIntExtra("IMAGEN",R.drawable.cafeloisl);
        String sNm = inDatosShow.getStringExtra("NOMBRE");
        String sDsc = inDatosShow.getStringExtra("DESCRIP");
        String sClle = inDatosShow.getStringExtra("DIRECCION");
        String sCol = inDatosShow.getStringExtra("COLONIA");
        String sTlf = inDatosShow.getStringExtra("TELEFONO");

        llDetalle.setBackgroundResource(iImgF);
        tvNombr.setText(sNm);
        tvDescr.setText(sDsc);
        tvCalle.setText(sClle);
        tvCol.setText(sCol);
        tvTel.setText(sTlf);
        inDial = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+sTlf));*/

    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        dfDetalle = (detalleFragment)fragment;

    }

    /*public void onClickTel(View v){
        startActivity(inDial);
    }*/
}
