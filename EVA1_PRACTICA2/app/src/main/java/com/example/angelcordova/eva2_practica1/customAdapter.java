package com.example.angelcordova.eva2_practica1;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Angel Córdova on 21/03/2017.
 */

public class customAdapter extends ArrayAdapter<DatosLista> {

    Context cntApp;
    int iLayout;
    DatosLista[] dlDatos;

    public customAdapter(Context context, int resource, DatosLista[] objects) {
        super(context, resource, objects);
        cntApp = context;
        iLayout = resource;
        dlDatos = objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vFila = convertView;
        ImageView ivRestSml;
        TextView tvNomb,tvDesc;

        if (vFila == null){
            //CREAR
            LayoutInflater liCrearLayout = ((Activity)cntApp).getLayoutInflater();
            vFila = liCrearLayout.inflate(iLayout,parent,false);
        }
        ivRestSml = (ImageView) vFila.findViewById(R.id.ivFondo);
        tvNomb = (TextView) vFila.findViewById(R.id.tvNom);
        tvDesc = (TextView) vFila.findViewById(R.id.tvDet);

        DatosLista dlRest = dlDatos[position];
        ivRestSml.setImageResource(dlRest.idImgSmall);
        tvNomb.setText(dlRest.sNom);
        tvDesc.setText(dlRest.sDes);

        return vFila;
    }
}
