package com.example.angelcordova.eva2_practica1;

/**
 * Created by Angel Córdova on 21/03/2017.
 */

public class DatosLista {
    String sNom, sDes, sDireccion, sColonia, sTelefono;
    int idImagen, idImgSmall;

    public DatosLista(String sNom, String sDes, int idImagen, String sDireccion, String sColonia, String sTelefono, int idImgSmall) {
        this.sNom = sNom;
        this.sDes = sDes;
        this.idImagen = idImagen;
        this.sDireccion = sDireccion;
        this.sColonia = sColonia;
        this.sTelefono = sTelefono;
        this.idImgSmall = idImgSmall;
    }
}
