package com.example.angelcordova.eva2_practica1;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class detalleFragment extends Fragment {

    LinearLayout llDetalle;
    TextView tvNombr, tvDescr, tvCalle, tvCol, tvTel;
    Intent inDial;
    View vLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public detalleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        vLayout = inflater.inflate(R.layout.fragment_detalle, container, false);
        llDetalle =(LinearLayout) vLayout.findViewById(R.id.activity_mostrar_detalle);
        tvNombr = (TextView) vLayout.findViewById(R.id.tvNombree);
        tvDescr = (TextView) vLayout.findViewById(R.id.tvDescrip);
        tvCalle = (TextView) vLayout.findViewById(R.id.tvCallee);
        tvCol = (TextView) vLayout.findViewById(R.id.tvColonia);
        tvTel = (TextView) vLayout.findViewById(R.id.tvTelef);


        /*Intent inDatosShow = pPrincipal.getIntent();
        int iImgF = inDatosShow.getIntExtra("IMAGEN",R.drawable.cafeloisl);
        String sNm = inDatosShow.getStringExtra("NOMBRE");
        String sDsc = inDatosShow.getStringExtra("DESCRIP");
        String sClle = inDatosShow.getStringExtra("DIRECCION");
        String sCol = inDatosShow.getStringExtra("COLONIA");
        String sTlf = inDatosShow.getStringExtra("TELEFONO");

        llDetalle.setBackgroundResource(iImgF);
        tvNombr.setText(sNm);
        tvDescr.setText(sDsc);
        tvCalle.setText(sClle);
        tvCol.setText(sCol);
        tvTel.setText(sTlf);
        inDial = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+sTlf));*/

        return vLayout;
    }

    public void onMsgFromMain(int iImg, String sName, String sDesc, String sCalle, String sCol, String sTel){
        if (vLayout != null){

            llDetalle.setBackgroundResource(iImg);
            tvNombr.setText(sName);
            tvDescr.setText(sDesc);
            tvCalle.setText(sCalle);
            tvCol.setText(sCol);
            tvTel.setText(sTel);
            inDial = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+sTel));
        }
    }

    public void onClickTel(View v){
        startActivity(inDial);
    }
}

