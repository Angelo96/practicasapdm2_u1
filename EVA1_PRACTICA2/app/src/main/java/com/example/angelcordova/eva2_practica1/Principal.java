package com.example.angelcordova.eva2_practica1;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class Principal extends AppCompatActivity {

    boolean bDualPanel;
    /*ListView lvListaR;
    DatosLista[] dlLista = {
            new DatosLista("Café Loisl","Una nueva experiencia ",R.drawable.cafeloisl,"Bistro Coriscano #1452","Mangolia Promenade","555-5925",R.drawable.cafeloisl_small),
            new DatosLista("Wings&Wings","Las mejores Alitas",R.drawable.alitas,"Pradera oculta #1556","Windenburg","250-7745",R.drawable.alitas_small),
            new DatosLista("Bistró L'Attente","Mariscos y más",R.drawable.mariscos,"Bistro Coriscano #1496","Mangolia Promenade","555-4532",R.drawable.mariscos_small),
            new DatosLista("Pizzas Antonelli","Pizza como en casa",R.drawable.pizzeria,"Valle de mayo #687","Willow Creek","450-5472",R.drawable.pizzeria_small),
            new DatosLista("WaffleHaus","YommYomii",R.drawable.wafflehaus,"Pradera oculta #1560","Windenburg","250-1456",R.drawable.wafflehaus_small),
            new DatosLista("Il piccolo caffè","Il caffè più delizioso",R.drawable.lecafe,"Bistro Coriscano #1350","Mangolia Promenade","555-8746",R.drawable.lecafe_small)
    };*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        /*lvListaR = (ListView) findViewById(R.id.lvLista);
        lvListaR.setAdapter(new customAdapter(this,R.layout.lista_restaurante,dlLista));
        lvListaR.setOnItemClickListener(this);*/
    }

    /*@Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent inDetRest = new Intent(this,MostrarDetalle.class);
        int iImagenF = dlLista[i].idImagen;
        String sNombreRest = dlLista[i].sNom;
        String sDescripcion = dlLista[i].sDes;
        String sCalleR = dlLista[i].sDireccion;
        String sColoniaR = dlLista[i].sColonia;
        String sTel = dlLista[i].sTelefono;

        inDetRest.putExtra("IMAGEN",iImagenF);
        inDetRest.putExtra("NOMBRE",sNombreRest);
        inDetRest.putExtra("DESCRIP",sDescripcion);
        inDetRest.putExtra("DIRECCION",sCalleR);
        inDetRest.putExtra("COLONIA",sColoniaR);
        inDetRest.putExtra("TELEFONO",sTel);

        startActivity(inDetRest);
    }*/

    public void onMsgFromFragToMain(int iImagenF, String sNombreRest, String sDescripcion, String sCalleR, String sColoniaR, String sTel){

        Intent inDetRest = new Intent(this,MostrarDetalle.class);
        inDetRest.putExtra("IMAGEN",iImagenF);
        inDetRest.putExtra("NOMBRE",sNombreRest);
        inDetRest.putExtra("DESCRIP",sDescripcion);
        inDetRest.putExtra("DIRECCION",sCalleR);
        inDetRest.putExtra("COLONIA",sColoniaR);
        inDetRest.putExtra("TELEFONO",sTel);

            View vDetalle = findViewById(R.id.layout_detalle);
            //si existe y se ve:
            bDualPanel = vDetalle != null && vDetalle.getVisibility() == View.VISIBLE;
            if (bDualPanel){//MODO LANDSCAPE
                detalleFragment dfFrag = (detalleFragment)getSupportFragmentManager().findFragmentById(R.id.layout_detalle);
                if(dfFrag == null){
                    dfFrag = new detalleFragment();
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.layout_detalle,dfFrag);
                    ft.commit();
                }
                dfFrag.onMsgFromMain(iImagenF,sNombreRest,sDescripcion,sCalleR,sColoniaR,sTel);
            } else { //MODO PORTRAIT
                //No hay espacio para el detalle asi que abre ptra actividad con el detalle
                startActivity(inDetRest);
            }
    }
}
