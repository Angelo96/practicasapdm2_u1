package com.example.angelcordova.eva2_practica1;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;


/**
 * A simple {@link Fragment} subclass.
 */
public class listaFragment extends Fragment {

    Principal pPrincipal;

    ListView lvListaR;
    DatosLista[] dlLista = {
            new DatosLista("Café Loisl","Una nueva experiencia ",R.drawable.cafeloisl,"Bistro Coriscano #1452","Mangolia Promenade","555-5925",R.drawable.cafeloisl_small),
            new DatosLista("Wings&Wings","Las mejores Alitas",R.drawable.alitas,"Pradera oculta #1556","Windenburg","250-7745",R.drawable.alitas_small),
            new DatosLista("Bistró L'Attente","Mariscos y más",R.drawable.mariscos,"Bistro Coriscano #1496","Mangolia Promenade","555-4532",R.drawable.mariscos_small),
            new DatosLista("Pizzas Antonelli","Pizza como en casa",R.drawable.pizzeria,"Valle de mayo #687","Willow Creek","450-5472",R.drawable.pizzeria_small),
            new DatosLista("WaffleHaus","YommYomii",R.drawable.wafflehaus,"Pradera oculta #1560","Windenburg","250-1456",R.drawable.wafflehaus_small),
            new DatosLista("Il piccolo caffè","Il caffè più delizioso",R.drawable.lecafe,"Bistro Coriscano #1350","Mangolia Promenade","555-8746",R.drawable.lecafe_small)
    };

    public listaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pPrincipal = (Principal) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View llVista = inflater.inflate(R.layout.fragment_lista, container, false);
        lvListaR = (ListView) llVista.findViewById(R.id.lvLista);
        lvListaR.setAdapter(new customAdapter(pPrincipal,R.layout.lista_restaurante,dlLista));
        lvListaR.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int iImagenF = dlLista[i].idImagen;
                String sNombreRest = dlLista[i].sNom;
                String sDescripcion = dlLista[i].sDes;
                String sCalleR = dlLista[i].sDireccion;
                String sColoniaR = dlLista[i].sColonia;
                String sTel = dlLista[i].sTelefono;

                pPrincipal.onMsgFromFragToMain(iImagenF, sNombreRest, sDescripcion, sCalleR, sColoniaR,sTel);
            }
        });
        return llVista;
    }

}
