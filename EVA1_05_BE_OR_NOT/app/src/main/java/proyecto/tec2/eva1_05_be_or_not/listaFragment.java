package proyecto.tec2.eva1_05_be_or_not;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class listaFragment extends Fragment {

    Main mMain;

    public listaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMain = (Main) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        LinearLayout llVista = (LinearLayout) inflater.inflate(R.layout.fragment_lista, container, false);
        ListView lvTitulos = (ListView)llVista.findViewById(R.id.lvTitulos);
        ArrayAdapter<String> aaShake = new ArrayAdapter(mMain, android.R.layout.simple_list_item_1, Shakespeare.TITLES);
        lvTitulos.setAdapter(aaShake);
        lvTitulos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //AQUI LLAMAMOS LA METODO EN PRINCIPAL
                mMain.onMsgFromFragToMain("TITU",i);
                Toast.makeText(mMain, Shakespeare.TITLES[i], Toast.LENGTH_SHORT).show();
            }
        });
        return llVista;
    }

}
