package proyecto.tec2.eva1_05_be_or_not;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class detalleFragment extends Fragment {

    TextView tvDetail;
    View vLayout;

    public detalleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        vLayout = inflater.inflate(R.layout.fragment_detalle, container, false);
        tvDetail = (TextView)vLayout.findViewById(R.id.tvDetalle);
        //LLENAR EL YTEXT VIEW ON INFORMACION

        return vLayout;
    }

    public void onMsgFromMain(int iIndex){
        if (vLayout != null){
            tvDetail.setText(Shakespeare.DIALOGUE[iIndex]);
        }
    }

}
