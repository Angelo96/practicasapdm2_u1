package proyecto.tec2.eva1_05_be_or_not;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Main extends AppCompatActivity {

    boolean bDualPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onMsgFromFragToMain(String sSender, int iMsg){
        if(sSender.equals("TITU")){ //FRAGMENTO CON LA LISTA
            /*vDetalle es el Framelayout del layout principal (landscape)
            Si  la app esta en modo land este frame ayout se crea
            si la app esta en modo porytrait este frame no existe
            */
            View vDetalle = findViewById(R.id.layout_detalle);
            //si existe y se ve:
            bDualPanel = vDetalle != null && vDetalle.getVisibility() == View.VISIBLE;
            if (bDualPanel){//MODO LANDSCAPE
                detalleFragment dfFrag = (detalleFragment)getSupportFragmentManager().findFragmentById(R.id.layout_detalle);
                if(dfFrag == null){
                    dfFrag = new detalleFragment();
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.layout_detalle,dfFrag);
                    ft.commit();
                }
                dfFrag.onMsgFromMain(iMsg);
            } else { //MODO PORTRAIT
                //No hay espacio para el detalle asi que abre ptra actividad con el detalle
                Intent iDetalle = new Intent(this, detalle.class);
                Bundle bDatos = new Bundle();
                bDatos.putInt("INDEX", iMsg);
                iDetalle.putExtras(bDatos);
                startActivity(iDetalle);
            }
        }
    }
}
