package proyecto.tec2.eva1_05_be_or_not;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class detalle extends AppCompatActivity {

    int iIndex;
    detalleFragment dfDetalle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);
        Intent inDatos = getIntent();
        Bundle bDato = inDatos.getExtras();
        iIndex = bDato.getInt("INDEX",0);
        dfDetalle.onMsgFromMain(iIndex);
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        dfDetalle = (detalleFragment)fragment;
        //dfDetalle.onMsgFromMain(iIndex);
    }
}
