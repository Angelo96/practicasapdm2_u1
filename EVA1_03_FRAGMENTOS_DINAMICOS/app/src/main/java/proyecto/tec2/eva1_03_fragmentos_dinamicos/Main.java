package proyecto.tec2.eva1_03_fragmentos_dinamicos;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Main extends AppCompatActivity {

    FragmentManager fmManager;
    FragmentTransaction ftTrans;
    blueFragment bfBlue;
    redFragment rfRed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fmManager = getSupportFragmentManager();
    }

    public void onClickBlue(View v){
        ftTrans = fmManager.beginTransaction();
        //CREAMOS EL FRAGMENTO
        bfBlue = new blueFragment();
        ftTrans.replace(R.id.layout_byes,bfBlue);
        ftTrans.commit();
    }
    public void onClickRed(View v){
        ftTrans = fmManager.beginTransaction();
        //CREAMOS EL FRAGMENTO
        rfRed = new redFragment();
        ftTrans.replace(R.id.layout_byes,rfRed);
        ftTrans.commit();
    }
}
