package proyecto.tec2.eva1_04_cambio_orientacion;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Main extends AppCompatActivity {

    boolean bPanelDual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onMsgFromFragToMain(String sSender, String sMsg){
        if(sSender.equals("UNO")){ //FRAGMENTO UNO GENERA LA LLAMADA
            View vwCambio = findViewById(R.id.fragmento_cambio);
            //SI ES FALSO ESTAMOS EN MODO LANDSCAPE
            bPanelDual = vwCambio != null && vwCambio.getVisibility() == View.VISIBLE;
            if (bPanelDual){//MODO LANDSCAPE
                //INSERTAR FRAGMENTO
                dosFragment dfFragDos = (dosFragment)getSupportFragmentManager().findFragmentById(R.id.fragmento_cambio);
                if(dfFragDos == null){//SI NO EXISTE LO CREAMOS
                    dfFragDos = new dosFragment();
                    //INICIAR TRANRACCION
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    //FINALIZARLA CON COMMIT
                    ft.replace(R.id.fragmento_cambio,dfFragDos);
                    ft.commit();
                }
            } else { //MODO PORTRAIT
                Intent inDetalle = new Intent(this, Detalle.class);
                startActivity(inDetalle);
            }

        }
    }
}
