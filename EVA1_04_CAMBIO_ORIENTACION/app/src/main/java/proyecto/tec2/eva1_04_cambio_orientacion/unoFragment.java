package proyecto.tec2.eva1_04_cambio_orientacion;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class unoFragment extends Fragment {

    Button btnShow;
    Main mMain;

    public unoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMain = (Main) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View  vwLayout = inflater.inflate(R.layout.fragment_uno, container, false);
        btnShow = (Button)vwLayout.findViewById(R.id.btnShow);
        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMain.onMsgFromFragToMain("UNO","¡Alolaaaaaaaa!");
            }
        });
        return vwLayout;
    }

}
