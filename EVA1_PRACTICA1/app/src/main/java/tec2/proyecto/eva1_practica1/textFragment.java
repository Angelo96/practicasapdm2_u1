package tec2.proyecto.eva1_practica1;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class textFragment extends Fragment {

    View view;
    TextView tvText;
    String sText = " One coat, two coats, I am being painted\n" +
            "Here and there I am being carved\n" +
            "Keep on painting me\n" +
            "\n" +
            "White, when I didn’t have you\n" +
            "I was white like a canvas\n" +
            "I was lonely but my days were peaceful, they were always\n" +
            "\n" +
            "Yellow, when you suddenly came to me\n" +
            "Before I knew it, my heart was filled\n" +
            "With warmth of the spring, pretty naturally\n" +
            "\n" +
            "I’m being painted one by one, like watercolors\n" +
            "I am being carved here and there, like a tattoo\n" +
            "Everywhere you touched me there’s a different color of\n" +
            "Memories that are coated over\n" +
            "It’s becoming darker and darker, like a black night\n" +
            "\n" +
            "Keep on painting me\n" +
            "With more memories\n" +
            "Hurry and paint me\n" +
            "With your scents\n" +
            "So that you can’t be erased\n" +
            "So that no one other than you can be drawn\n" +
            "In my heart\n" +
            "\n" +
            "Red, the moment you held me passionately\n" +
            "It erupted like a volcano, my heart\n" +
            "There’s a red remnant in my heart, it’s still warm\n" +
            "\n" +
            "Blue, the day I cried for the first time because of you\n" +
            "Like the ocean with unknowable depths\n" +
            "It’s been drawn so blue,\n" +
            "Do you remember?\n" +
            "\n" +
            "I’m being painted one by one, like watercolors\n" +
            "I am being carved here and there, like a tattoo\n" +
            "Everywhere you touched me\n" +
            "There’s a different color of\n" +
            "Memories that are coated over\n" +
            "It’s becoming darker and darker\n" +
            "Like a black night\n" +
            "\n" +
            "Keep on painting me\n" +
            "With more memories\n" +
            "Hurry and paint me\n" +
            "With your scents\n" +
            "So that you can’t be erased\n" +
            "So that no one other than you can be drawn\n" +
            "In my heart\n" +
            "\n" +
            "It’s okay if I turn blacker and blacker\n" +
            "Our memories\n" +
            "It’s becoming thicker and thicker, like espresso\n" +
            "\n" +
            "Yeah keep on coating me\n" +
            "Don’t stop yourself\n" +
            "I am meaningless without you\n" +
            "Fill me up completely\n" +
            "So that there are no empty spaces\n" +
            "You are my only artist ";

    public textFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_text, container, false);
        tvText = (TextView) view.findViewById(R.id.tvText);
        tvText.setText(sText);

        return view;
    }

}
