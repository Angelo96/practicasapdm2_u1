package tec2.proyecto.eva1_practica1;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;

public class Mai extends AppCompatActivity {

    RadioButton rbText, rbPicture, rbSong;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    textFragment fragmentText;
    pictureFragment fragmentPicture;
    audioFragment fragmentAudio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mai);
        rbText = findViewById(R.id.rbText);
        rbPicture = findViewById(R.id.rbPicture);
        rbSong = findViewById(R.id.rbSong);
        fragmentManager = getSupportFragmentManager();

        rbText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickText();
            }
        });
        rbPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickPicture();
            }
        });
        rbSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickAudio();
            }
        });


    }

    private void onClickText(){
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentText = new textFragment();
        fragmentTransaction.replace(R.id.frmLyTarget,fragmentText);
        fragmentTransaction.commit();

    }

    private void onClickPicture(){
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentPicture = new pictureFragment();
        fragmentTransaction.replace(R.id.frmLyTarget,fragmentPicture);
        fragmentTransaction.commit();

    }

    private void onClickAudio(){
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentAudio = new audioFragment();
        fragmentTransaction.replace(R.id.frmLyTarget,fragmentAudio);
        fragmentTransaction.commit();
    }
}
